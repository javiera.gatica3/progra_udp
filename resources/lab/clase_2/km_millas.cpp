#include <iostream>
using namespace std;

int main(){
	//constantes
	const float km_to_millas = 0.621371;
	const float millas_to_km = 1.6093;

 	//variables
	int opt =0;
	float valor=0;
	float resultado=0;

	//calculo
	cout<<"Desea:"<<endl;
	cout<<"1- km a millas"<<endl;
	cout<<"2- millas a km"<<endl;
	cin >> opt;
	cout<<"Ingrese valor"<<endl;
	cin >> valor;
	if (opt == 1){
		resultado=valor*km_to_millas;
	}
	if (opt == 2){
		resultado=valor*millas_to_km;
	}

	//resultado
	cout<<"El resultado es: "<< resultado <<endl;
	return 0;
}
