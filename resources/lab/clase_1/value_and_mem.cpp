#include <iostream>
using namespace std;

int main(){
	int numero; //Asingnación de la variable
	int *puntero; //Asignación de un puntero
	int numero2;

	numero =16;
	puntero=&numero;//Asignación de la dirección de memoria.
	numero2=*puntero;//Recuperar el valor de una dirección de memoria
	*puntero=*puntero+20; //Suma 20 al puntero q apunta a la dir de mem de "numero"

	cout<<"Numero :"<<numero<<endl;
	cout<<"Dir Mem :"<<puntero<<endl; //Imprime dirección de memoria
	cout<<"Puntero :"<<numero2<<endl; //Imprime dirección de memoria

	return 0;
}
	
