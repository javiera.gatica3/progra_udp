#include <iostream>

using namespace std;

int main(int, char **)
{
    char c = 'A';
    int x = int(c); // Look ma! No cast!
    cout << "The character '" << c << "' has an ASCII code of " << x << endl;
    return 0;
}
