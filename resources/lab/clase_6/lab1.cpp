#include <iostream>
#include <string>
using namespace std;

int main(){

  const float kg_pan = 2190;
  const float kg_queso = 5429;
  const float kg_jamon = 10800;
  const float unidad_huevo = 200;
  const float kg_mantequilla = 15445;

  string opt ="";
  float kg=0;
  float total=0;

  cout<<"Tienda La Esquina"<<endl;
  cout<<"Ingrese el producto"<<endl;
  cout<<"1 - Pan"<<endl;
  cout<<"2 - Queso"<<endl;
  cout<<"3 - Jamon "<<endl;
  cout<<"4 - Huevos"<<endl;
  cout<<"5 - Mantequilla"<<endl;
  cout<<"Ingrese una opcion: ";
  cin >> opt;
  cout<<"Ingrese los unidades o kg: ";
  cin >> kg;

  if (opt == "1" || opt == "pan"){
    total = kg_pan * kg;
    if (kg > 1.5){
      total = total -300;
    } 
  } else if (opt == "2" || opt == "queso"){
    total = kg_queso * kg;
    if (kg > 2){
      total = total -1000;
    }
  } else if (opt == "3" || opt == "jamon"){
    total = kg_jamon * kg;
  } else if (opt == "4" || opt == "huevos"){
    int cantidad = kg; // cantidad de huevos, solo es un cambio de nombre de la variable
    int bandejas30 = cantidad / 30;
    cout<<"Bandejas: "<<bandejas30<<endl;
    int descuento = 700 * bandejas30;
    cout<<"Descuento: "<<descuento<<endl;
    total = ( unidad_huevo * cantidad ) ;
    cout<<"total: "<<total<<endl;
    total = total - descuento;
    cout<<"total con descuento: "<<total<<endl;
  } else if (opt == "5" || opt == "mantequilla"){
    total = kg_mantequilla * kg;
    if (kg >= 1){
      total = total -300;
    }
  } else {
    cout << "Producto no existe!"<<endl;
    return 0;
  }

  if (total > 10000){
    total =total - 300;
  }

  cout<<"El total a pagar es: "<<total<<endl;

  return 0;
}

