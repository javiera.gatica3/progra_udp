#include <iostream>
#include <string>
using namespace std;



int main(){

	const int pan_con_palta = 1800;
	const float p_despacho = 0.1;
	const float p_descuento = 0.93;
	float cantidad, total = 0;
	string despacho = "false";

	cout <<"CANTIDAD DE PANES A COMPRAR: ";
	cin >> cantidad;

	cout <<"Despacho a domicilio? [true/false]: ";
	cin >> despacho;

	total = (pan_con_palta*cantidad);
	if (cantidad > 10){
		total = (pan_con_palta*cantidad)*p_descuento;
	}

	if (despacho == "true"){
		total += (pan_con_palta*cantidad)*p_despacho;
	}

	cout.precision(20); // digitos a mostrar 
	cout <<"Total a Pagar: "<<total<<endl;

	return 0;
}
